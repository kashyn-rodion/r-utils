#!/bin/bash

# Current script can parse named arguments, provided in format: <name>=<value>.
#
# How to use.
#
# You must declare and assign two variables:
#   declare -A REQUIRED_ARGS
#   declare -A OPTIONAL_ARGS
#   REQUIRED_ARGS - associative array REQUIRED_ARGS[<argument name>]='<description>'
#   OPTIONAL_ARGS - associative array OPTIONAL_ARGS[<argument name>]='<description>'
#
# Optionaly you can provide array with defaults values for optional arguments:
#   OPTIONAL_DEFAULTS - associative array OPTIONAL_DEFAULTS[<argument name>]=<value>
#
# Optionaly you can declare SCRIPT_DESCRIPTION variable to add it to top of help message.
#
# After that include current source like this:
#   source named-arg-parser.sh
#
# If all required arguments are passed then you can use them as regular variable by name.
# If some required arguments are missed, then help message will be printed and then script will be stopped.
#
# dependencies:
#     grep
#     sed
#     seq
#
# Author Kashyn R.
# Repository: https://bitbucket.org/kashyn-rodion/r-utils/

function print_help()
{
    if [[ ! -z "$SCRIPT_DESCRIPTION" ]];
    then
        echo -e "$SCRIPT_DESCRIPTION";
        echo '';
    fi
    echo "Argument list."
    if [[ ! -z "${!REQUIRED_ARGS[@]}" ]];
    then
        echo "  Required:"
        for key in "${!REQUIRED_ARGS[@]}"
        do
            echo "    $key: ${REQUIRED_ARGS[$key]}"
        done
    fi

    if [[ ! -z "${!OPTIONAL_ARGS[@]}" ]];
    then
        echo "  Optional:"
        for key in "${!OPTIONAL_ARGS[@]}"
        do
            paramDefinition="    $key: "
            indent=$(printf "%0.s " $(seq 1 ${#paramDefinition}))
            description=$(echo "${OPTIONAL_ARGS[$key]}" | sed ":a;N;\$!ba;s/\n/\n${indent}/g")
            echo -e "${paramDefinition}${description}"
            if [[ ! -z "${OPTIONAL_DEFAULTS[$key]}" ]];
            then
                echo "${indent}Default: '${OPTIONAL_DEFAULTS[$key]}'."
            fi
        done
    fi
}

# parse required args
if [[ ! -z "${!REQUIRED_ARGS[@]}" ]]
then
    for key in "${!REQUIRED_ARGS[@]}"
    do
        for ((i=1; i<=$#; i++))
        do
            val=$(echo "${!i}" | grep -oP "$key=\K(.*)")
            if [ ! -z "$val" ];
            then
                break;
            fi
        done

        if [[ -z "$val" ]];
        then
            print_help
            exit 1;
        else
            eval "$key='$val'"
        fi
    done
fi

if [[ ! -z "${!OPTIONAL_ARGS[@]}" ]]
then
    for key in "${!OPTIONAL_ARGS[@]}"
    do
        for ((i=1; i<=$#; i++))
        do
            val=$(echo "${!i}" | grep -oP "$key=\K(.*)")
            if [ ! -z "$val" ];
            then
                break;
            fi
        done

        if [[ ! -z "$val" ]];
        then
            eval "$key='$val'"
        else
            if [[ ! -z "${OPTIONAL_DEFAULTS[$key]}" ]];
            then
                eval "$key='${OPTIONAL_DEFAULTS[$key]}'"
            fi
        fi
    done
fi
